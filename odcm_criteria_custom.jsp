<%@ include file="/custom/commonInclude.jsp" %>
<%
    ContextManager ARJspCurr =(ContextManager)session.getAttribute("CustomARJspCurr");
	String sGroupName = ARJspCurr.getCurrentGroup();
	String funcCode = (String)ARJspCurr.getInput(sGroupName+".funcCode", "");
	SecurityInfo70 securityInfo = (SecurityInfo70)session.getAttribute("FinUserInfo");
%>
<script language="javascript" src="../../javascripts/common_functions.js" ></script>
<script language="javascript" src="../../javascripts/cust_post_evt.js" ></script>
<script language="javascript" src="../../javascripts/cust_pre_evt.js" ></script>

<script language="javascript">

var funcCode = "<%=ParseValue.checkString(((String)ARJspCurr.getInputWithGroup("mode","")))%>";
var BODDate= '<%= ParseValue.checkString((securityInfo.bodDate).substring(0,10))%>';
var noConfirm = false;


function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}else{
		var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}
}

function readCookie(name)
{ 
	var nameEQ = name + "=";	
	var ca = document.cookie.split(';');	
	for(var i=0;i < ca.length;i++) {	
	var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}	
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}
var count = 0;
var docpageVisited = "false";
var isAllocated = "N";

//function popVal(){
//	var notify = objForm.notify.value;
//	if (!!notify){
		
//	}	

//}


//function hasAllocation(isAllocated){
//	var objForm = document.forms[0];
//	var dcCurrency = objForm.dcCurrency.value.toUpperCase();
//	var funcCode = objForm.funcCode.value;
//	var cifId = objForm.cifId.value.toUpperCase();
//	var formNo = objForm.formNo.value.toUpperCase();
//	
//	var input ="formNo|"+formNo+"|App|5|frmType|M|billCcy|"+dcCurrency;
//	var outputNames = "flg|Acct";
//	var retVal = appFnExecuteScript(input,outputNames,"FormValidate.scr",false);
//	if ((retVal !== "") && (retVal !== "undefined") && (retVal !== null) && (retVal !== ''))
//	{
//		var flg = retVal.split("|")[1];
//		
//		if ((flg == 'E') || (flg == 'D')) {
//			var Acct = retVal.split("|")[3];
//			if(!!Acct){
//				eraseCookie("ALLOCACCT");
//				createCookie("ALLOCACCT",Acct,0);
//			}
//		}
//	
//		isAllocated = flg;
//		return isAllocated;
//	}
//	
//
//}


function odcm_criteria_pre_ONCLICK(obj){
	frm = document.forms[0];
	var check = "0";
	var checkr = "0";
	var checka = "null";
	if(mopId == "ODCM"){	
		var sof=frm.sof.value.toUpperCase();
		var tranId = frm.tranId.value.toUpperCase();
		var funcCode = frm.funcCode.value;
		var cifId = frm.cifId.value;
		var solId = frm.solId.value;
		var alloctAmt = objForm.allocateAmt.value;
		var odcmNum = frm.odcmNum.value.toUpperCase();
		var formNo = frm.formNo.value.toUpperCase();
		var newFormNo = frm.newFormNo.value.toUpperCase();
		var dcCcy = frm.dcCurrency.value;
		var odcmType = frm.odcmType.value.toUpperCase();
		var typeOfDc="";
		if(document.forms[0].typeOfDC[0].checked==true)
		{	
			typeOfDc = document.forms[0].typeOfDC[0].value;		
		}else{	 
			typeOfDc = document.forms[0].typeOfDC[1].value;				
		}
		var formReq="";
		if(document.forms[0].formReq[0].checked==true)
		{	
			formReq = document.forms[0].formReq[0].value;		
		}else{	 
			formReq = document.forms[0].formReq[1].value;				
		}

		var notify="";
		if(document.forms[0].notify[0].checked==true)
		{	
			notify = document.forms[0].notify[0].value;		
		}else{	 
			notify = document.forms[0].notify[1].value;				
		}
		
		if (!!cifId){
			eraseCookie("CIFID");
			createCookie("CIFID",cifId,0);
		}
		
		if (!!tranId){
			eraseCookie("DEALID");
			createCookie("DEALID",tranId,0);
		}
		if (!!odcmType){
			eraseCookie("REGTYPE");
			createCookie("REGTYPE",odcmType,0);
		}
		if (!!formNo){
			eraseCookie("FORMNO");
			createCookie("FORMNO",formNo,0);
		}
		if (!!funcCode){
			eraseCookie("FUNCODE");
			createCookie("FUNCODE",funcCode,0);
		}
		
		if ((funcCode == "M")||(funcCode == "C")){
			newFormNo = frm.newFormNo.value.toUpperCase();
			if (formReq == "Y"){
				showImage("sLnk103");
				enableFields('tranId');
				eraseCookie("FUNCODE");
				createCookie("FUNCODE",funcCode,0);
				eraseCookie("FORMREQ");
				createCookie("FORMREQ",formReq,0);
				if (funcCode == "C"){
				eraseCookie("FORMNO");
				createCookie("FORMNO",newFormNo,0);
				}else{
				eraseCookie("FORMNO");
				createCookie("FORMNO",formNo,0);
				}
				eraseCookie("REGTYPE");
				createCookie("REGTYPE",odcmType,0);
				eraseCookie("NOTIFY");
				createCookie("NOTIFY",notify,0);
				eraseCookie("CHECK");
				createCookie("CHECK",check,0);
				
			}
		}	
		
		if (funcCode == "V"){
			count = NG_fnOnChangeDcNo(count);
			if (count == "1"){
				return false
			}
		}
				
		if ((funcCode == "S")||(funcCode == 'C')||(funcCode == "M")||(funcCode == "V")||(funcCode == "R")||(funcCode == "A")){
			eraseCookie("LCNUM");
			createCookie("LCNUM",odcmNum,0);
			
			if ((formReq == "Y")&&(obj.id == "Go")){
				eraseCookie("FORMNO");
				if (funcCode == "C"){
					createCookie("FORMNO",newFormNo,0);	
				}else{
					createCookie("FORMNO",formNo,0);
				}
				eraseCookie("NOTIFY");
				createCookie("NOTIFY",notify,0);
				var formNo = readCookie("FORMNO");
				var input = "App|27|formNo|"+formNo;
				var outputNames = "flg|fxFlg";
				var retVal = appFnExecuteScript(input,outputNames,"FormValidate.scr",false);	
				if ((retVal !== "") && (retVal !== "undefined") && (retVal !== null) && (retVal !== ''))
				{
					var flg = retVal.split("|")[1];
					var val3 = retVal.split("|")[3];
					if (flg == 'Y'){
						alert(val3);
						return false;
					}
				}
			}
		}	
		if ((funcCode == "V")&&(sof == "Y")&&(obj.id == "Go")&&(typeOfDc == "C")&&(odcmType == "FCCLC")){
			if (tranId == ""){
				alert("Please Enter Transaction ID for CashBack LC");
				return false;
			}
		}		
		if ((funcCode == "V")&&(obj.id == "Go")&&(typeOfDc == "C")&&(odcmType == "FCCLC")){
			if (sof == ""){
				alert("Please select the source of fund for CashBack LC");
				return false;
			}
		}	
		alert(alloctAmt);
		if ((funcCode == "V")&&(sof == "Y")&&(obj.id == "Go")&&(typeOfDc == "U")&&(odcmType == "IMPU1")&&(alloctAmt != "0.00")){
			if (tranId == ""){
				showImage("sLnk103");
				enableFields('tranId');
				alert("Please Enter Transaction ID for CashBacking Unconfirmed LC");
				return false;
			}
		}		
		if ((funcCode == "V")&&(obj.id == "Go")&&(typeOfDc == "U")&&(odcmType == "IMPU1")&&(alloctAmt != "0.00")){
			if (sof == ""){
				showImage("sLnk103");
				enableFields('tranId');
				alert("Please select the source of fund for CashBacking Unconfirmed LC");
				return false;
			}
		}	
		if ((funcCode == "S")&&(obj.id == "Go")&&(odcmType == "IMPU1")){
			if (typeOfDc == "C"){
				alert("Register type is Unconfirmed \n DC Type must also me Unconfirmed");
				return false;
			}
		}
		
		if ((funcCode == "S")||(funcCode == 'C')){
			if (formReq == "Y"){
				eraseCookie("FUNCODE");
				createCookie("FUNCODE",funcCode,0);
				eraseCookie("FORMREQ");
				createCookie("FORMREQ",formReq,0);
				eraseCookie("FORMNO");
				if (funcCode == "C"){
					createCookie("FORMNO",newFormNo,0);	
				}else{
					createCookie("FORMNO",formNo,0);
				}
				eraseCookie("REGTYPE");
				createCookie("REGTYPE",odcmType,0);
				eraseCookie("CIFID");
				createCookie("CIFID",cifId,0);
				eraseCookie("NOTIFY");
				createCookie("NOTIFY",notify,0);
				eraseCookie("SOF");
				createCookie("SOF",sof,0);
				eraseCookie("CHECK");
				createCookie("CHECK",check,0);
				eraseCookie("CHECKA");
				createCookie("CHECKA",checka,0);
				eraseCookie("CHECKR");
				createCookie("CHECKR",checkr,0);
				//alert(sof);
				if (!!cifId){
					eraseCookie("CIFID");
					createCookie("CIFID",cifId,0);
				}
				if (!!odcmType){
					eraseCookie("REGTYPE");
					createCookie("REGTYPE",odcmType,0);
				}
				if ((sof == "Y")&&(obj.id == "Go")&&(typeOfDc == "C")&&(odcmType == "FCCLC")){
					if (tranId == ""){
						alert("Please Enter Transaction ID for CashBack LC");
						return false;
					}
				}
				if ((sof == "")&&(obj.id == "Go")&&(typeOfDc == "C")&&(odcmType == "FCCLC")){
						alert("Please select the source of fund for CashBack LC");
						return false;
				}
		
				if ((funcCode == "C")&&(obj.id == "Go")){
					var input ="formNo|"+newFormNo+"|App|3";
				
				}else{
					if ((funcCode == "S")&&(obj.id == "Go")){
						var input ="formNo|"+formNo+"|App|3";
					}
				}
				var outputNames = "flg|msg";
				var retVal = appFnExecuteScript(input,outputNames,"FormValidate.scr",false);
				if ((retVal !== "") && (retVal !== "undefined") && (retVal !== null) && (retVal !== ''))
				{
					var flg = retVal.split("|")[1];
					var msg = retVal.split("|")[3];
					if (flg == 'N'){
						alert( msg);
						return false;
					}

				}
			}
			if ((odcmType == "IMPCL") || (odcmType == "IMPU1") || (odcmType == "FCCLC")){
			//	if (odcmNum == ""){
			//		generateNextnum();
			//	}
				
				var odcmType = frm.odcmType.value.toUpperCase();

				if (odcmType == "IMPCL"){
					var frm = document.forms[0];
					var cifId = frm.cifId.value;
					var solId = frm.solId.value;
					if (funcCode == "C"){
						var formNo = frm.newFormNo.value;					
					}else{
						var formNo = frm.formNo.value;
					}
					var offshoreRate = frm.offshoreRate.value;
					var bankRate = frm.bankRate.value;
					var modAppli = frm.modAppli.value;
					var valueDate = frm.valueDate.value;
					var lcCrncy = frm.dcCurrency.value;
					var LcNum = frm.odcmNum.value;
					var odcmType = frm.odcmType.value;
					var odcmType = frm.odcmType.value.toUpperCase();

					document.getElementById('cleanline').style.display = "";
					if(cust_fnIsNull(frm.offshoreRate.value))
					{
						alert("Please Enter Offshore Rate");
						document.forms[0].offshoreRate.focus();
						return false;	
					}

					if(frm.modAppli[1].checked == false && frm.modAppli[0].checked == false)
					{
					alert("Please Pick Mode of Application !!!");
					return false;	
					}
						
					if(cust_fnIsNull(frm.bankRate.value))
					{
						alert("Please Enter Bank Rate");
						document.forms[0].bankRate.focus();
						return false;	
					}
						
					if(cust_fnIsNull(frm.valueDate.value))
					{
						//alert(modAppli);
						alert("Please Enter Value Date");
						document.forms[0].valueDate.focus();
						return false;	
					}
					
					if (!!cifId){
					//alert(bankRate);
					//alert(offshoreRate);
					//alert(solId);
						eraseCookie("CIFID");
						createCookie("CIFID",cifId,0);
						eraseCookie("FORMNUM");
						if (funcCode == "C"){
							createCookie("FORMNUM",newFormNo,0);				
						}else{
							createCookie("FORMNUM",formNo,0);
						}
						eraseCookie("OFFSHORERATE");
						createCookie("OFFSHORERATE",offshoreRate,0);
						eraseCookie("BANKRATE");
						createCookie("BANKRATE",bankRate,0);
						eraseCookie("MODAPPLI");
						createCookie("MODAPPLI",modAppli,0);
						eraseCookie("VALDATE");
						createCookie("VALDATE",valueDate,0);
						eraseCookie("SOLID");
						createCookie("SOLID",solId,0);
						eraseCookie("CURRENCY");
						createCookie("CURRENCY",lcCrncy,0);
						eraseCookie("LCNUM");
						createCookie("LCNUM",odcmNum,0);
						eraseCookie("ODCMTYPE");
						createCookie("ODCMTYPE",odcmType,0);
					}
				}
			}
			var odcmType = frm.odcmType.value.toUpperCase();
			
			if(formReq == "Y")
			{
				if (odcmType == 'FCCLC'){
					//isAllocated = hasAllocation(isAllocated);
					isAllocated = sof;
					//alert(isAllocated);
					if ((isAllocated == 'Y')||(isAllocated == 'D')||(isAllocated == 'E')){
						eraseCookie("ISALLOCATED");
						createCookie("ISALLOCATED",isAllocated,0);
						eraseCookie("DCCY");
						createCookie("DCCY",dcCcy,0);
						if ((isAllocated == 'D')||(isAllocated == 'E')){
							hideImage("sLnk103");
							disableFields('tranId');
						}else{
							showImage("sLnk103");
							enableFields('tranId');
						}
					}else{
						if (isAllocated == 'FL'){	
							alert("No fund has been allocated to this form");
							return false;
						}else{
							if (isAllocated == 'N'){
								eraseCookie("ISALLOCATED");
								createCookie("ISALLOCATED",isAllocated,0);
							}
			
						}
					}
				}
			}
		}
	}
}

function odcm_criteria_ONCHANGE(p1,obj) {
	var odcmType = frm.odcmType.value.toUpperCase();
	if (odcmType == "IMPCL"){
		document.getElementById('cleanline').style.display = "";
		if ((obj.id == "offshoreRate") || (obj.id == "bankRate")){
			ValidateRate();
		}
	}
}

function ValidateRate()
{
	if(mopId == "ODCM")
	{
		if (odcmType == "IMPCL"){
			if(isNaN(document.forms[0].offshoreRate.value))
			{
				alert("Invalid Rate amount");
				document.forms[0].offshoreRate.focus();
				return false;
			}
			
			if(isNaN(document.forms[0].bankRate.value))
			{
				alert("Invalid Rate amount");
				document.forms[0].bankRate.focus();
				return false;
			}
		}
	}
}
/*
function generateNextnum(){
	frm = document.forms[0];
	var odcmType = frm.odcmType.value.toUpperCase();
	if (!!odcmType){
		var input ="regType|"+odcmType+"|App|3";
		var outputNames = "flg|msg";
		var retVal = appFnExecuteScript(input,outputNames,"getNextNumber.scr",false);
		if ((retVal !== "") && (retVal !== "undefined") && (retVal !== null) && (retVal !== ''))
		{
			var flg = retVal.split("|")[1];
			var nextNum = retVal.split("|")[3];
			if (flg == 'Y'){
				 frm.odcmNum.value = nextNum;
			}

		}
	}
}
*/

function odcm_criteria_pre_ONBLUR(obj,p1,p2)
{
	var retVal = "";
	if (pre_ONBLUR('odcm_criteria',obj) == false) { 
		return false;
	}
	if ((retVal =  fetchDetails()) == false) {
		return false;
	}
	if ((retVal = onBlurFormatDate(p1)) == false) {
		return false;
	}
	if ((retVal = fnAssignDateOnEnter(p2)) == false) {
		return false;
	}
	if (post_ONBLUR('odcm_criteria',obj) == false) { 
		return false;
	}
	return (retVal == undefined) ? true : retVal;
}


function NG_fnOnChangeDcNo(count)
{
	count = count + 1;
    if((objForm.odcmNum.value != "") && (objForm.funcCode.value != "S"))
    {
        var eventId         = objForm.odcmNum.value.toUpperCase();
        var inputNameValues = "eventId|"+eventId+"|eventIdType|DOCCR";
        var outputNames     = "cifId|formNo|formCrncyCode|allocAmt|utilAmt|regType|typeOfDC";
        var scrName         = "popFormId.scr";
        var retVal          = fnExecuteLocaleScript(objForm,inputNameValues,outputNames,scrName,false);
        var val1            = retVal.split("|")[1];
        var val2            = retVal.split("|")[3];
        var val3            = retVal.split("|")[5];
        var val4            = retVal.split("|")[7];
        var val5            = retVal.split("|")[9];
        var val6            = retVal.split("|")[11];
		var val7            = retVal.split("|")[13];
		var val8            = retVal.split("|")[15];
		var val9            = retVal.split("|")[17];
		//alert(val7);
        objForm.cifId.value                 = val1;
        objForm.formNo.value                = val2;
        objForm.dcCurrency.value            = val3;
        objForm.allocateCrncyCode.value     = val3;
        objForm.utilizedCrncyCode.value     = val3;
        objForm.confirmedCrncyCode.value   	= val3;
        objForm.allocateAmt.value           = val4;
        newformatAmt(format, objForm.allocateAmt, allocateCrncyCode, 'N');
        objForm.utilizedAmt.value           = val5;
        newformatAmt(format, objForm.utilizedAmt, utilizedCrncyCode, 'N');
        objForm.odcmType.value             = val6;

        if(objForm.formNo.value != "")
            checkRadio(objForm.formReq,'Y');
        else
            checkRadio(objForm.formReq,'N');
			
		
		if(val7 == "U")
        {
            checkRadio(objForm.typeOfDC,'U');
            if(objForm.funcCode.value=="M")
            {
                if(val8=="N")
                {
                    enableFields("newFormNo");
                    showImage("sLnk102");
                    disableFields("tranId");
                    hideImage("sLnk103");
                    disableFields("confirmedAmt");

                }
                if(val8=="Y")
                {
                    disableFields("newFormNo");
                    hideImage("sLnk102");
                    enableFields("tranId");
					showImage("sLnk103");
                    enableFields("confirmedAmt");
                }
				if(val9 == "Y")
				{
					  showImage("sLnk103");
					  enableFields('tranId');
				}
				if(val9 == "N")
				{
					    hideImage("sLnk103");
						disableFields('tranId');
				}
            }
			else
			{
				hideImage("sLnk103");
				disableFields('tranId');
			}
        }
       else
		{	
            checkRadio(objForm.typeOfDC,'C');
			if((objForm.funcCode.value == "V") || (objForm.funcCode.value == "A") || (objForm.funcCode.value == "M") || (objForm.funcCode.value == "C")  )
			{
				if(objForm.funcCode.value == "V") {
					fcode = "V"
					eraseCookie("FUNCODE");
					createCookie("FUNCODE",fcode,0);
				}
			}
		}
			disableFields('formNo','cifId','dcCurrency');
			hideImage("sLnk6");
			hideImage("sLnk30");
			hideImage("sLnk19");
			hideImage("sLnk101");
        return count;
    }
    return count;
}



function getForwardRecords()
{	
	objForm = document.forms[0];
	if (funcCode == "C"){
		var frmNum = objForm.newFormNo.value;				
	}else{
		var frmNum = objForm.formNo.value;
	}
	var inputNameValues = "step|1|frmNum|"+frmNum;
	var outputNames = "frmType|frmNum|allocAmt|allocRate|fCcy|credAcct|dealId|dealDate|maturityDate|fwdTenor|sof|applFlg";
	var scrName = "popFundingDetails.scr";
	var title = "FORWARD DETAILS";
	var literalNames = "FORM TYPE" + "|" + "FORM NUMBER" + "|" + "ALLOCATED AMOUNT" + "|" + "ALLOCATION RATE"+ "|" + "CURRENCY" + "|" + "CREDIT ACCOUNT" + "|" + "DEAL ID" + "|" + "DEAL DATE" + "|" + "MATURITY DATE" + "|" + "FORWARD TENOR" + "|" + "SOURCE OF FUND"+ "|" + "APPLIED FLAG";
	var hyperLnks = "1";
	
	var retVal = fnExecuteLocaleScriptForList(objForm,inputNameValues, outputNames, scrName,title, literalNames, '1', true);
}


function getTreasuryBookings()
{	
	objForm = document.forms[0];
	if (funcCode == "C"){
		var frmNum = objForm.newFormNo.value;				
	}else{
		var frmNum = objForm.formNo.value;
	}
	var inputNameValues = "step|2|frmNum|"+frmNum;
	var outputNames = "frmType|frmNum|allocAmt|allocRate|fCcy|credAcct|dealId|dealDate|maturityDate|fwdTenor|sof|applFlg";
	var scrName = "popFundingDetails.scr";
	var title = "TREASURY SPOT BOOKING DETAILS";
	var literalNames = "FORM TYPE" + "|" + "FORM NUMBER" + "|" + "ALLOCATED AMOUNT" + "|" + "ALLOCATION RATE"+ "|" + "CURRENCY" + "|" + "CREDIT ACCOUNT" + "|" + "DEAL ID" + "|" + "DEAL DATE" + "|" + "MATURITY DATE" + "|" + "FORWARD TENOR" + "|" + "SOURCE OF FUND" + "|" + "APPLIED FLAG";
	var hyperLnks = "1";
	
	var retVal = fnExecuteLocaleScriptForList(objForm,inputNameValues, outputNames, scrName,title, literalNames, '1', true);
}

function getFundingReport()
{	
	objForm = document.forms[0];
	if (funcCode == "C"){
		var frmNum = objForm.newFormNo.value;				
	}else{
		var frmNum = objForm.formNo.value;
	}
	var inputNameValues = "step|3|frmNum|"+frmNum;
	var outputNames = "frmType|frmNum|reason|rDate";
	var scrName = "popFundingDetails.scr";
	var title = "TREASURY FUNDING REPORT";
	var literalNames = "FORM TYPE" + "|" + "FORM NUMBER" + "|" + "REASON" + "|" + "FUND DATE";
	var hyperLnks = "1";
	
	var retVal = fnExecuteLocaleScriptForList(objForm,inputNameValues, outputNames, scrName,title, literalNames, '1', true);
}

</script>


<script>

with (document){
		write('<html>');
		write('<table width="100%"  border="5px" cellspacing="0" cellpadding="10px" class="ctable">');
		write('<tr>');
		write('<td>');
		write('<table width="100%" border="0" cellpadding="0" cellspacing="0" class="innertable">');
		write('<tr id="notifyTbl">');
		write('<td class="textlabel" style="height: 20px">SEND NOTIFICATION</td>');
		write('<td class="textfield">');
		write('<input type="radio" checked="true" name="notify" id="notify" value="Y"  style="width: 40px" onblur="popVal()">Yes');
		write('<input type="radio" name="notify" id="notify" value="N" style="width: 40px" onblur="popVal()">No');
		write('</td>');
		write('<td class="columnwth"></td>');
		write('<td class="textlabel" style="height: 20px">SOURCE OF FUND</td>');
		write('<td class="textfield">');
		write('<select name="sof" id="sof" class="listboxfont" >');
		write('<option value="">---SELECT---</option>');
		write('<option value="Y">CBN</option>');
		write('<option value="D">DOM</option>');
		write('<option value="E">EXPORT PROCEED</option>');
		write('<option value="O">OTHRS</option>');
		write('</select>');
		write('</td>');
		write('</tr>');
		
		write('<tr id="fwd">');
		write('<td class="textlabel" style="height: 15px">VIEW FORWARD BOOKING DETAILS ON FORM M</td>');
		write('<td class="textfield">');
		write('<a href="javascript:getForwardRecords()">');
		write('<img border="0" height="17" id="sLnk33" hotKeyId="search1" src="../Renderer/images/'+applangcode+'/search_icon.gif" width="16">');
		write('</a>');
		write('</td>');
		write('<td class="columnwth"></td>');
		write('<td class="textlabel" style="height: 15px">VIEW TREASURY BOOKING DETAILS ON FORM M</td>');
		write('<td class="textfield">');
		write('<a href="javascript:getTreasuryBookings()">');
		write('<img border="0" height="17" id="sLnk33" hotKeyId="search1" src="../Renderer/images/'+applangcode+'/search_icon.gif" width="16">');
		write('</a>');
		write('</td>');
		write('</tr>');
		
		write('<tr id="report">');
		write('<td class="textlabel" style="height: 15px">VIEW FUNDING REPORT FORM M</td>');
		write('<td class="textfield">');
		write('<a href="javascript:getFundingReport()">');
		write('<img border="0" height="17" id="sLnk33" hotKeyId="search1" src="../Renderer/images/'+applangcode+'/search_icon.gif" width="16">');
		write('</a>');
		write('<td class="textfield"> </td>');
		write('<td class="columnwidth"> </td>');
		write('<td class="textfield"> </td>');
		write('<td class="columnwidth"> </td>');
		write('</td>');
		write('</tr>');
	
		write('</table>');
		write('</td>');
		write('</tr>');
		write('</table>');
	
		write('<input type="hidden" id="valueDate" fdt="fdate" mneb1="N" vFldId="valueDate_ui" name="valueDate">');
		write('<table width="100%"  border="0" cellspacing="0" cellpadding="0" class="ctable">');
		write('<tr>');
		write('<td>');
		write('<tr id="cleanline" style="display:none">');
		write('<td>');
		write('<table border="0" cellpadding="0" cellspacing="0" width="100%">');
		write('<tr>');
		write('<td valign="top">');
		write('<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">');
		write('<tr>');
		write('<td>');
		write('<table width="100%" border="0" cellpadding="0" cellspacing="0" class="innertable">');
		
		write('<tr >');
		write('<td class="textlabel" style="height: 15px">OFFSHORE RATE</td>');
		write('<td class="textfield">');
		write('<input type="text" class="textfieldfont" name="offshoreRate" id="offshoreRate" onblur="ValidateRate()">');
		write('</td>');
		write('<td class="columnwth"></td>');
		write('<td class="textlabel" style="height: 15px"> MODE OF APPLICATION</td>');
		write('<td class="textfield">');
		write('<select name="modAppli" id="modAppli" class="listboxfont" >');
		write('<option value="">---SELECT---</option>');
		write('<option value="M">MONTHLY</option>');
		write('</select>');
		write('</td>');
		write('</tr>');
		write('<tr>');
		write('<td class="textlabel" style="height: 15px">MARGIN RATE</td>');
		write('<td class="textfield">');
		write('<input type="text" class="textfieldfont" name="bankRate" id="bankRate" onblur="ValidateRate()">');
		write('</td>');
		write('<td class="columnwth"></td>');
		write('<td class="textlabel" style="height: 15px">VALUE DATE</td>');
		write('<td class="textfield"><input onBlur="javascript:return odcm_criteria_pre_ONBLUR(this,this,this);" id="valueDate_ui" name="valueDate_ui" hotKeyId="calender3" type="text" class="textfieldfont" maxlength="10" mnebl="false" fdt="uidate" fblk="defaultFblk1" onChange="javascript:return odcm_criteria_pre_ONBLUR(this,this,this);">');
		write('<a  href="javascript:openDate(document.forms[0].valueDate_ui,BODDate)"   id="sLnk2"><img align="absmiddle" alt="Date picker" border="0" height="19"  hotKeyId="calender1" src="../Renderer/images/'+applangcode+'/calender.gif" width="24" class="img" >');
		write('</a>');
		write('</td>');	
		write('</tr>');

		write('</table>');
		write('</td>');
		write('</tr>');
		write('</table>');
		write('</td>');
		write('</tr>');
		write('</table>');
		write('</td>');
		write('</tr>');
		write('</table>');
		write('</html>');
	}
</script>
