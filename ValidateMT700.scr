#===============================================================
# IMPORTANT: Auto Generated Header. DO NOT CHANGE MANUALLY.
# category: Script Name Based
# script: ValidateMT700.scr
# event: NA
# description: ValidateMT700
#===============================================================
############################################################################################
#  Script Name : ValidateMT700.scr (Validation Script for SWIFT Message MT700)             #
#  Description : This script does the validations like mandatory fields,  co-existence,    #
#                co-mandatory etc.. and network validations. All the common field level    #
#                validations are done through the script, ValidateSwiftTag.scr which is    #
#                called from this script.                                                  #
############################################################################################
<--start
trace on
IF (BANCS.STDIN.userId == "FIVUSR") THEN
CALLSCRIPTIFEXIST("FI_ValidateMT700.scr")
exitscript 
ENDIF

sv_r = urhk_B2k_printRepos("BANCS")
#
##-------------------Check for Mandatory Fields------------------------------#  
#Mandatory check for 27
if((BANCS.INPUT.tag271 == "") OR (BANCS.INPUT.tag272 == "")) then
    BANCS.OUTPUT.errorMessage = " All 27 fields must be entered."
    BANCS.OUTPUT.failedFieldTag = "tag271"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#Mandatory check for 40
if((BANCS.INPUT.tag40a1 == "")) then
    BANCS.OUTPUT.errorMessage = " Field 40A must be entered."
    BANCS.OUTPUT.failedFieldTag = "tag40a1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#Mandatory check for 40e
BANCS.INPUT.tag40e1 = LTRIM(RTRIM(BANCS.INPUT.tag40e1))
if ((BANCS.INPUT.tag40e1 != "UCP LATEST VERSION") AND (BANCS.INPUT.tag40e1 != "EUCP LATEST VERSION") AND (BANCS.INPUT.tag40e1 !="UCPURR LATEST VERSION") AND (BANCS.INPUT.tag40e1 != "EUCPURR LATEST VERSION") AND (BANCS.INPUT.tag40e1 != "ISP LATEST VERSION") AND (BANCS.INPUT.tag40e1 != "OTHR")) then
    BANCS.OUTPUT.errorMessage = "  Valid values for 40e field are UCP LATEST VERSION, EUCP LATEST VERSION, UCPURR LATEST VERSION, EUCPURR LATEST VERSION, ISP LATEST VERSION, OTHR."
    BANCS.OUTPUT.failedFieldTag = "tag40e1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
if ((BANCS.INPUT.tag40e1 == "OTHR") AND (BANCS.INPUT.tag40e2 == "")) then
    BANCS.OUTPUT.errorMessage = "  Applicable sub rules detail must be entered when rule is OTHR."
    BANCS.OUTPUT.failedFieldTag = "tag40e2"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
if ((BANCS.INPUT.tag40e1 != "OTHR") AND (BANCS.INPUT.tag40e2 != "")) then
    BANCS.OUTPUT.errorMessage = "  Applicable sub rules detail must be entered only when rule is OTHR."
    BANCS.OUTPUT.failedFieldTag = "tag40e2"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#Mandotory check for 31D
if((BANCS.INPUT.tag31d1 == "") OR (BANCS.INPUT.tag31d2 == "")) then
    BANCS.OUTPUT.errorMessage = " All 31D fields must be entered."
    BANCS.OUTPUT.failedFieldTag = "tag31d1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#Mandotory check for 50
if ((BANCS.INPUT.tag501 == "") AND (BANCS.INPUT.tag502 == "") AND (BANCS.INPUT.tag503 == "") AND (BANCS.INPUT.tag504 == "")) then
    BANCS.OUTPUT.errorMessage = "Field 50 must be entered."
    BANCS.OUTPUT.failedFieldTag = "tag501"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#Mandatory check for 59
if ((BANCS.INPUT.tag592 == "") OR (BANCS.INPUT.tag593 == "")) then
    BANCS.OUTPUT.errorMessage = "Field 59 must be entered."
    BANCS.OUTPUT.failedFieldTag = "tag591"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
if ((BANCS.INPUT.tag591 != "") AND ((LEFT$(BANCS.INPUT.tag591,1) != "/") or (MID$(BANCS.INPUT.tag591,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
    BANCS.OUTPUT.failedFieldTag = "tag591"
	BANCS.OUTPUT.successOrFailure = "F"
	exitscript 
endif	

# This script is used to validate the IBAN Number entered in the
# field 59(tag591)

if (BANCS.INPUT.tag591 != "") then
	sv_b = urhk_getFileLocation("SCRIPT|validateIBAN.scr")
	if (sv_b != 1) then
		BANCS.INPUT.valIBANreq = "N"
		if (LEFT$(BANCS.INPUT.tag591,1) == "/") then
			sv_z = ((STRLEN(BANCS.INPUT.tag591)) - 1)
			BANCS.INPUT.IBANNumber = MID$(BANCS.INPUT.tag591,1,sv_z) 
		else
			BANCS.INPUT.IBANNumber = BANCS.INPUT.tag591
		endif
		call(BANCS.OUTPARAM.fileLocation, "validateIBAN.scr")
		if(BANCS.OUTPUT.successOrFailure == "F") then
    		BANCS.OUTPUT.errorMessage = "Enter a valid Beneficiary IBAN  - Field 591"
			BANCS.OUTPUT.failedFieldTag = "tag591"
			exitscript
		endif
	endif
endif


#
#
if ((BANCS.INPUT.tag41a1 == "") AND (BANCS.INPUT.tag41a2 == "") AND (BANCS.INPUT.tag41d1 == "") AND (BANCS.INPUT.tag41d2 == "") AND (BANCS.INPUT.tag41d3 == "") AND (BANCS.INPUT.tag41d4 == "") AND (BANCS.INPUT.tag41d5 == "")) then
    BANCS.OUTPUT.errorMessage = "Field 41 must be entered."
    BANCS.OUTPUT.failedFieldTag = "tag41a1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#Mandatory check for 49
if( (BANCS.INPUT.tag491 == "") ) then
    BANCS.OUTPUT.errorMessage = "Field 49 must be entered."
    BANCS.OUTPUT.failedFieldTag = "tag491"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#--------------------End Of Mandatory Fields Validations--------------------##
#
##-------------------Check for Common Validations----------------------------#
# common Validation for 20 and 21
sv_c = urhk_getFileLocation("SCRIPT|ValidateSwiftTag.scr")
if (sv_c == 1) then
   BANCS.OUTPUT.errorMessage = "Validation Script [ValidateSwiftTag.scr] does not exists"
   BANCS.OUTPUT.failedFieldTag = ""
   BANCS.OUTPUT.successOrFailure = "F"
   exitscript
endif
call(BANCS.OUTPARAM.fileLocation, "ValidateSwiftTag.scr")
if(BANCS.OUTPUT.successOrFailure == "F") then
    exitscript
endif


#--------------------End of Common Validations------------------------------##
#
##-------------------Field Level Message Specific Validations----------------#
#Checking the Valid value for the field 40a1
BANCS.INPUT.tag40a1 = LTRIM(RTRIM(BANCS.INPUT.tag40a1))
#
if((BANCS.INPUT.tag40a1 != "REVOCABLE") AND (BANCS.INPUT.tag40a1 != "IRREVOCABLE") AND (BANCS.INPUT.tag40a1 != "REVOCABLE STANDBY") AND (BANCS.INPUT.tag40a1 != "IRREVOCABLE STANDBY") AND (BANCS.INPUT.tag40a1 != "REVOCABLE TRANSFERABLE") AND (BANCS.INPUT.tag40a1 != "IRREVOCABLE TRANSFERABLE") AND (BANCS.INPUT.tag40a1 != "IRREVOC TRANS STANDBY") ) then
    BANCS.OUTPUT.errorMessage = " Valid values for 40A field are REVOCABLE, IRREVOCABLE, REVOCABLE STANDBY, IRREVOCABLE STANDBY, REVOCABLE TRANSFERABLE,IRREVOCABLE TRANSFERABLE and IRREVOC TRANS STANDBY."
    BANCS.OUTPUT.failedFieldTag = "tag40a1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#Checking the Valid value for the field 231
BANCS.INPUT.tag231 = LTRIM(RTRIM(BANCS.INPUT.tag231))
#
sv_x = MID$(BANCS.INPUT.tag231,0,7)
#
if((BANCS.INPUT.tag231 != "") AND (sv_x != "PREADV/")) then
    BANCS.OUTPUT.errorMessage = "Tag23 field must contain the code PREADV followed by a slash `/' and a reference to preadvice."
    BANCS.OUTPUT.failedFieldTag = "tag231"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#Checking the Valid value for the field 39b1
BANCS.INPUT.tag39b1 = LTRIM(RTRIM(BANCS.INPUT.tag39b1))
#
if ((BANCS.INPUT.tag39b1 != "") AND (BANCS.INPUT.tag39b1 != "NOT EXCEEDING")) then
    BANCS.OUTPUT.errorMessage = "Valid value for 39B field is NOT EXCEEDING ."
    BANCS.OUTPUT.failedFieldTag = "tag39b1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#Checking the Valid value for the field 41a2
BANCS.INPUT.tag41a2 = LTRIM(RTRIM(BANCS.INPUT.tag41a2))
#
if((BANCS.INPUT.tag41a2 != "" ) AND (BANCS.INPUT.tag41a2 != "BY ACCEPTANCE" ) AND (BANCS.INPUT.tag41a2 != "BY DEF PAYMENT" ) AND (BANCS.INPUT.tag41a2 != "BY MIXED PYMT" ) AND (BANCS.INPUT.tag41a2 != "BY NEGOTIATION" ) AND (BANCS.INPUT.tag41a2 != "BY PAYMENT" ) ) then
    BANCS.OUTPUT.errorMessage = " Valid values for 41A code field are BY ACCEPTANCE, BY DEF PAYMENT,BY MIXED PYMT, BY NEGOTIATION and BY PAYMENT."
    BANCS.OUTPUT.failedFieldTag = "tag41a2"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
# Checking the Valid values for field 41d5
BANCS.INPUT.tag41d5 = LTRIM(RTRIM(BANCS.INPUT.tag41d5))
#
if((BANCS.INPUT.tag41d5 != "" ) AND (BANCS.INPUT.tag41d5 != "BY ACCEPTANCE" ) AND (BANCS.INPUT.tag41d5 != "BY DEF PAYMENT" ) AND (BANCS.INPUT.tag41d5 != "BY MIXED PYMT" ) AND (BANCS.INPUT.tag41d5 != "BY NEGOTIATION" ) AND (BANCS.INPUT.tag41d5 != "BY PAYMENT" ) ) then
    BANCS.OUTPUT.errorMessage = " Valid values for 41D code field are BY ACCEPTANCE, BY DEF PAYMENT,BY MIXED PYMT, BY NEGOTIATION and BY PAYMENT."
    BANCS.OUTPUT.failedFieldTag = "tag41d5"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#Checking the Valid value for the field 491
BANCS.INPUT.tag491 = LTRIM(RTRIM(BANCS.INPUT.tag491))
#
if((BANCS.INPUT.tag491 != "CONFIRM") AND (BANCS.INPUT.tag491 != "MAY ADD") AND (BANCS.INPUT.tag491 != "WITHOUT")) then
    BANCS.OUTPUT.errorMessage = "Valid values for 49 field are CONFIRM, MAY ADD and WITHOUT Only."
    BANCS.OUTPUT.failedFieldTag = "tag491"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
#
# Checking for A or D option fields 
# Either Field 51A should be entered or field 51D.
if (((BANCS.INPUT.tag51a1 != "") OR (BANCS.INPUT.tag51a2 != "")) AND ((BANCS.INPUT.tag51d1 != "") OR (BANCS.INPUT.tag51d2 != "") OR (BANCS.INPUT.tag51d3 != "") OR (BANCS.INPUT.tag51d4 != "") OR (BANCS.INPUT.tag51d5 != ""))) then
	BANCS.OUTPUT.errorMessage = "Either 51A or 51D fields can be entered, but not both."
    BANCS.OUTPUT.failedFieldTag = "tag51a1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
# If Party Id is specified, then it should start with a "/" and should not be null.
if ((BANCS.INPUT.tag51a1 != "") AND ((LEFT$(BANCS.INPUT.tag51a1,1) != "/") or (MID$(BANCS.INPUT.tag51a1,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
	BANCS.OUTPUT.failedFieldTag = "tag51a1"
    BANCS.OUTPUT.successOrFailure = "F"
	exitscript
endif	
#
# If Party Id for option D is specified, then it should start with a "/" and should not be null.
if ((BANCS.INPUT.tag51d1 != "") AND ((LEFT$(BANCS.INPUT.tag51d1,1) != "/") or (MID$(BANCS.INPUT.tag51d1,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
    BANCS.OUTPUT.failedFieldTag = "tag51d1"
	BANCS.OUTPUT.successOrFailure = "F"
	exitscript 
endif	
#
#
#
# Checking for A or D fields
if (((BANCS.INPUT.tag41a1 != "") OR (BANCS.INPUT.tag41a2 != "")) AND ((BANCS.INPUT.tag41d1 != "") OR (BANCS.INPUT.tag41d2 != "") OR (BANCS.INPUT.tag41d3 != "") OR (BANCS.INPUT.tag41d4 != "") OR (BANCS.INPUT.tag41d5 != ""))) then
	BANCS.OUTPUT.errorMessage = "Either 41A or 41D fields can be entered, but not both."
    BANCS.OUTPUT.failedFieldTag = "tag41a1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
# Both BIC and Code have to be entered or none.
if ( ((BANCS.INPUT.tag41a1 != "") AND (BANCS.INPUT.tag41a2 == "")) OR  ((BANCS.INPUT.tag41a1 == "") AND (BANCS.INPUT.tag41a2 != "")) ) then
    if (BANCS.INPUT.tag41a1 == "" ) then
	    BANCS.OUTPUT.failedFieldTag = "tag41a1" 
    else
	    BANCS.OUTPUT.failedFieldTag = "tag41a2"
	endif	 
	BANCS.OUTPUT.errorMessage = "Either both BIC and Code should be entered or none can be entered."
	BANCS.OUTPUT.successOrFailure = "F" 
	exitscript
endif
#
# Both Name, Address & Code have to be entered or none.
if ( ((BANCS.INPUT.tag41d1 == "") AND (BANCS.INPUT.tag41d2 == "") AND (BANCS.INPUT.tag41d3 == "") AND (BANCS.INPUT.tag41d4 == "" ) AND (BANCS.INPUT.tag41d5 != "")) OR (((BANCS.INPUT.tag41d1 != "") OR (BANCS.INPUT.tag41d2 != "") OR (BANCS.INPUT.tag41d3 != "") OR (BANCS.INPUT.tag41d4 != "")) AND (BANCS.INPUT.tag41d5 == "")) ) then 
    if (BANCS.INPUT.tag41d5 == "") then 
        BANCS.OUTPUT.failedFieldTag =  "tag41d5"
	else	
        BANCS.OUTPUT.failedFieldTag =  "tag41d1"
	endif
    BANCS.OUTPUT.errorMessage = "Either Name/Address & code both have to be entered or none can be entered."
	BANCS.OUTPUT.successOrFailure = "F"
	exitscript
endif
#
#
# Checking for tag42 A or D fields 
if (((BANCS.INPUT.tag42a1 != "") OR (BANCS.INPUT.tag42a2 != "")) AND ((BANCS.INPUT.tag42d1 != "") OR (BANCS.INPUT.tag42d2 != "") OR (BANCS.INPUT.tag42d3 != "") OR (BANCS.INPUT.tag42d4 != "") OR (BANCS.INPUT.tag42d5 != ""))) then
	BANCS.OUTPUT.errorMessage = "Either 42A or 42D fields can be entered, but not both."
    BANCS.OUTPUT.failedFieldTag = "tag42a1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
# If Party Id is specified, then it should start with a "/" and should not be null.
if ((BANCS.INPUT.tag42a1 != "") AND ((LEFT$(BANCS.INPUT.tag42a1,1) != "/") or (MID$(BANCS.INPUT.tag42a1,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
	BANCS.OUTPUT.failedFieldTag = "tag42a1"
    BANCS.OUTPUT.successOrFailure = "F"
	exitscript
endif	
#
# If Party Id for option D is specified, then it should start with a "/" and should not be null.
if ((BANCS.INPUT.tag42d1 != "") AND ((LEFT$(BANCS.INPUT.tag42d1,1) != "/") or (MID$(BANCS.INPUT.tag42d1,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
    BANCS.OUTPUT.failedFieldTag = "tag42d1"
	BANCS.OUTPUT.successOrFailure = "F"
	exitscript 
endif	
#
#
#
# Checking for A or D fields
# Either Field 53A should be entered or field 53D.
if (((BANCS.INPUT.tag53a1 != "") OR (BANCS.INPUT.tag53a2 != "")) AND ((BANCS.INPUT.tag53d1 != "") OR (BANCS.INPUT.tag53d2 != "") OR (BANCS.INPUT.tag53d3 != "") OR (BANCS.INPUT.tag53d4 != "") OR (BANCS.INPUT.tag53d5 != ""))) then
	BANCS.OUTPUT.errorMessage = "Either 53A or 53D fields can be entered, but not both."
    BANCS.OUTPUT.failedFieldTag = "tag53a1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
# If Party Id is specified, then it should start with a "/" and should not be null.
if ((BANCS.INPUT.tag53a1 != "") AND ((LEFT$(BANCS.INPUT.tag53a1,1) != "/") or (MID$(BANCS.INPUT.tag53a1,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
	BANCS.OUTPUT.failedFieldTag = "tag53a1"
    BANCS.OUTPUT.successOrFailure = "F"
	exitscript
endif	
#
# If Party Id for option D is specified, then it should start with a "/" and should not be null.
if ((BANCS.INPUT.tag53d1 != "") AND ((LEFT$(BANCS.INPUT.tag53d1,1) != "/") or (MID$(BANCS.INPUT.tag53d1,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
    BANCS.OUTPUT.failedFieldTag = "tag53d1"
	BANCS.OUTPUT.successOrFailure = "F"
	exitscript 
endif	
#
#
# Checking for A or B or D fields
sv_h = 0
#
if ((BANCS.INPUT.tag57a1 != "") OR (BANCS.INPUT.tag57a2 != "")) then
    sv_h = sv_h + 1
endif
#
if ((BANCS.INPUT.tag57b1 != "") OR (BANCS.INPUT.tag57b2 != "")) then
    sv_h = sv_h + 1
endif
#
if ((BANCS.INPUT.tag57d1 != "") OR (BANCS.INPUT.tag57d2 != "") OR (BANCS.INPUT.tag57d3 != "")        OR (BANCS.INPUT.tag57d4 != "") OR (BANCS.INPUT.tag57d5 != "")) then
    sv_h = sv_h + 1
endif
#
if (sv_h > 1) then
    BANCS.OUTPUT.errorMessage = "Only one set of 57A,57B or 57D fields can be entered."
    BANCS.OUTPUT.failedFieldTag = "tag57a1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
# If Party Id is specified, then it should start with a "/" and should not be null.
if ((BANCS.INPUT.tag57a1 != "") AND ((LEFT$(BANCS.INPUT.tag57a1,1) != "/") or (MID$(BANCS.INPUT.tag57a1,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
	BANCS.OUTPUT.failedFieldTag = "tag57a1"
    BANCS.OUTPUT.successOrFailure = "F"
	exitscript
endif	
#
#
# If Party Id is specified, then it should start with a "/" and should not be null.
if ((BANCS.INPUT.tag57b1 != "") AND ((LEFT$(BANCS.INPUT.tag57b1,1) != "/") or (MID$(BANCS.INPUT.tag57b1,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
	BANCS.OUTPUT.failedFieldTag = "tag57b1"
    BANCS.OUTPUT.successOrFailure = "F"
	exitscript
endif	
#
# If Party Id for option D is specified, then it should start with a "/" and should not be null.
if ((BANCS.INPUT.tag57d1 != "") AND ((LEFT$(BANCS.INPUT.tag57d1,1) != "/") or (MID$(BANCS.INPUT.tag57d1,1,1) == ""))) then
    BANCS.OUTPUT.errorMessage = "Acct/Party identifier should not be null and should be preceded with /"
    BANCS.OUTPUT.failedFieldTag = "tag57d1"
	BANCS.OUTPUT.successOrFailure = "F"
	exitscript 
endif	
#
#
#
#--------------------End of Field Level Validations-------------------------##
#
##-------------------Message Level Network Validations-----------------------#
# Checking the validation for "Either field 39A or 39B, but not both, may be present."
sv_d = CINT(BANCS.INPUT.tag39a1)
sv_e = CINT(BANCS.INPUT.tag39a2)

if ( ((sv_d != 0) OR (sv_e != 0)) AND (BANCS.INPUT.tag39b1 != "")) then
	BANCS.OUTPUT.errorMessage = "Either 39A or 39B fields can be entered, but not both."
	BANCS.OUTPUT.failedFieldTag = "tag39a1"
	BANCS.OUTPUT.successOrFailure = "F"
	exitscript
endif
#
#
#
# Checking the validation for "When used, fields 42C and 42A must both be present "
sv_a = 0
#
if((BANCS.INPUT.tag42a1 != "") OR (BANCS.INPUT.tag42a2 != "") OR (BANCS.INPUT.tag42d1 != "") OR (BANCS.INPUT.tag42d2 != "") OR (BANCS.INPUT.tag42d3 != "") OR (BANCS.INPUT.tag42d4 != "") OR (BANCS.INPUT.tag42d5 != "")  ) then
    sv_a = 1 
endif
#
if ((((BANCS.INPUT.tag42c1 == "") AND (BANCS.INPUT.tag42c2 == "") AND (BANCS.INPUT.tag42c3 == "")) AND (sv_a == 1)) OR (((BANCS.INPUT.tag42c1 != "") OR (BANCS.INPUT.tag42c2 != "") OR (BANCS.INPUT.tag42c3 != "")) AND (sv_a == 0)))  then
	BANCS.OUTPUT.errorMessage = "When used, fields 42C and 42A both must be present ."
	BANCS.OUTPUT.failedFieldTag = "tag42c1"
	BANCS.OUTPUT.successOrFailure = "F"
	exitscript
endif
#
#
#
# Checking the validation for "Either fields 42C and 42a together, or field 42M alone, or field 42P alone may be present. No other combination of these fields is allowed "
sv_c = 0
#
if((BANCS.INPUT.tag42c1 != "") OR (BANCS.INPUT.tag42c2 != "") OR (BANCS.INPUT.tag42c3 != "") ) then
    sv_c = sv_c +1 
endif
#
if((BANCS.INPUT.tag42m1 != "") OR (BANCS.INPUT.tag42m2 != "") OR (BANCS.INPUT.tag42m3 != "") OR (BANCS.INPUT.tag42m4 != "") ) then
    sv_c = sv_c +1
endif
#
if((BANCS.INPUT.tag42p1 != "") OR (BANCS.INPUT.tag42p2 != "") OR (BANCS.INPUT.tag42p3 != "") OR (BANCS.INPUT.tag42p4 != "") ) then
    sv_c = sv_c +1
endif
#
if (sv_c > 1) then
    BANCS.OUTPUT.errorMessage = "Only one set of 42C and 42a,42M or 42P fields can be entered."
    BANCS.OUTPUT.failedFieldTag = "tag42c1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#
#
#
# Checking the validation for "Either field 44C or 44D, but not both, may be present"
if ((BANCS.INPUT.tag44c1 != "") AND (BANCS.INPUT.tag44d1 != ""))  then
	BANCS.OUTPUT.errorMessage = "Either 44C or 44D fields can be entered, but not both."
	BANCS.OUTPUT.failedFieldTag = "tag44c1"
	BANCS.OUTPUT.successOrFailure = "F"
	exitscript
endif
#
#
#
# Checking the validation for fields 42M and 42P. 
# If sub-field Available By of field 41A has a value 
if((BANCS.INPUT.tag41a2 == "BY MIXED PYMT") OR (BANCS.INPUT.tag41d5 == "BY MIXED PYMT")) then 
   if((BANCS.INPUT.tag42m1 == "") AND (BANCS.INPUT.tag42m2 == "") AND (BANCS.INPUT.tag42m3 == "") AND (BANCS.INPUT.tag42m4 == "")) then 
        BANCS.OUTPUT.errorMessage = "Tag 42M has to be entered during Mixed Payment" 
	    BANCS.OUTPUT.failedFieldTag = "tag42m1"
		BANCS.OUTPUT.successOrFailure = "F"
	    exitscript
   endif
endif
if((BANCS.INPUT.tag41a2 == "BY DEF PAYMENT") OR (BANCS.INPUT.tag41d5 == "BY DEF PAYMENT")) then 
   if((BANCS.INPUT.tag42p1 == "") AND (BANCS.INPUT.tag42p2 == "") AND (BANCS.INPUT.tag42p3 == "") AND (BANCS.INPUT.tag42p4 == "")) then 
        BANCS.OUTPUT.errorMessage = "Tag 42P has to be entered during Deferred Payment" 
	    BANCS.OUTPUT.failedFieldTag = "tag42p1"
		BANCS.OUTPUT.successOrFailure = "F"
	    exitscript
   endif
endif

#--------------------------------Fetching Values ---------------------------##
if ((BANCS.INPUT.tag721 != "") OR (BANCS.INPUT.tag722 != "") OR (BANCS.INPUT.tag723 != "") OR (BANCS.INPUT.tag724 != "") OR (BANCS.INPUT.tag725 != "") OR (BANCS.INPUT.tag726 != "")) then
#{
BANCS.INPARAM.tag721 = BANCS.INPUT.tag721
BANCS.INPARAM.tag722 = BANCS.INPUT.tag722
BANCS.INPARAM.tag723 = BANCS.INPUT.tag723
BANCS.INPARAM.tag724 = BANCS.INPUT.tag724
BANCS.INPARAM.tag725 = BANCS.INPUT.tag725
BANCS.INPARAM.tag726 = BANCS.INPUT.tag726
BANCS.INPARAM.tagName = "tag72"
BANCS.INPARAM.MTnum = "700"
#-----------------------calling the function for validation-----------------##
sv_r = urhk_ValidateCodesForTag("")
sv_a = BANCS.OUTPARAM.SuccFailFlg
if (sv_r != 0) then
    BANCS.OUTPUT.errorMessage = "TAG 72 Validation Failed"
	BANCS.OUTPUT.failedFieldTag = "tag721" 
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#}
endif

if ((BANCS.INPUT.tag71b1 != "") OR (BANCS.INPUT.tag71b2 != "") OR (BANCS.INPUT.tag71b3 != "") OR (BANCS.INPUT.tag71b4 != "") OR (BANCS.INPUT.tag71b5 != "") OR (BANCS.INPUT.tag71b6 != "")) then
#{
BANCS.INPARAM.tag71b1 = BANCS.INPUT.tag71b1
BANCS.INPARAM.tag71b2 = BANCS.INPUT.tag71b2
BANCS.INPARAM.tag71b3 = BANCS.INPUT.tag71b3
BANCS.INPARAM.tag71b4 = BANCS.INPUT.tag71b4
BANCS.INPARAM.tag71b5 = BANCS.INPUT.tag71b5
BANCS.INPARAM.tag71b6 = BANCS.INPUT.tag71b6
BANCS.INPARAM.tagName = "tag71b"
BANCS.INPARAM.MTnum = "700"
#-----------------------calling the function for validation-----------------##
sv_r = urhk_ValidateCodesForTag("")
sv_a = BANCS.OUTPARAM.SuccFailFlg
if (sv_r != 0) then
    BANCS.OUTPUT.errorMessage = "TAG 71b Validation Failed"
	BANCS.OUTPUT.failedFieldTag = "tag71b1"
    BANCS.OUTPUT.successOrFailure = "F"
    exitscript
endif
#}
endif
#--------------------End Of Network Level Validations-----------------------##
#
BANCS.OUTPUT.errorMessage = ""
BANCS.OUTPUT.failedFieldTag = ""
BANCS.OUTPUT.successOrFailure = "S"
#
trace off
exitscript
end-->
